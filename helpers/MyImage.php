<?php
namespace app\helpers;

class MyImage
{
    /**
     * Save base64 as file
     * @param $image - image on base64
     * @param string $path - save path
     * @return file name
     */
    function base64Save($image, $path = 'images/')
    {
        $imageName = uniqid() . '.jpeg';

        $imageFile = fopen($path . $imageName, 'wb');
        $data = explode(',', $image);
        fwrite($imageFile, base64_decode($data[1]));
        fclose($imageFile);

        return $imageName;
    }


}
