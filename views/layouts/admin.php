<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Url;
use yii\helpers\Html;
?>

<?php $this->beginPage() ?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Znews | Dashboard</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>

    <link rel="shortcut icon" type="image/x-icon" href="images/favicon.ico">
    <?php $this->head() ?>

    <!-- Font Awesome -->
    <?php $this->registerCssFile('@web/AdminLTE/plugins/fontawesome-free/css/all.min.css') ?>
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Tempusdominus Bbootstrap 4 -->
    <?php $this->registerCssFile('@web/AdminLTE/plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css') ?>
    <!-- iCheck -->
    <?php $this->registerCssFile('@web/AdminLTE/plugins/icheck-bootstrap/icheck-bootstrap.min.css') ?>

    <!-- Theme style -->
    <?php $this->registerCssFile('@web/AdminLTE/dist/css/adminlte.min.css') ?>
    <!-- overlayScrollbars -->
    <?php $this->registerCssFile('@web/AdminLTE/plugins/overlayScrollbars/css/OverlayScrollbars.min.css') ?>
    <?php $this->registerCssFile('@web/AdminLTE/plugins/datatables-bs4/css/dataTables.bootstrap4.css') ?>

    <?php $this->registerCssFile('@web/AdminLTE/plugins/sweetalert2-theme-bootstrap-4/bootstrap-4.min.css') ?>

    <?php $this->registerCssFile('@web/AdminLTE/plugins/croppie/croppie.css') ?>

    <?php $this->registerCssFile('@web/plugins/fancybox/dist/jquery.fancybox.min.css') ?>
    <?php $this->registerCssFile('@web/css/admin-css.css') ?>
    <?php $this->registerCssFile('@web/css/sizes.css') ?>

    <!-- Google Font: Source Sans Pro -->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
    <?php $this->registerCssFile('@web/plugins/Validation-Engine/validationEngine.jquery.css') ?>
    <!-- jQuery -->
    <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
    <?php $this->registerJsFile('@web/AdminLTE/plugins/datatables/jquery.dataTables.js') ?>

    <?php $this->registerJsFile('@web/AdminLTE/plugins/datatables-bs4/js/dataTables.bootstrap4.js') ?>


</head>
<body class="hold-transition sidebar-mini layout-fixed">
<?php $this->beginBody() ?>

<div class="wrapper">

    <!-- Navbar -->
    <nav class="main-header navbar navbar-expand navbar-white navbar-light">
        <!-- Left navbar links -->
        <ul class="navbar-nav">
            <li class="nav-item">
                <a class="nav-link" data-widget="pushmenu" href="#"><i class="fas fa-bars"></i></a>
            </li>
        </ul>

        <!-- Right navbar links -->
        <ul class="navbar-nav ml-auto">
        </ul>
    </nav>
    <!-- /.navbar -->

    <!-- Main Sidebar Container -->
    <aside class="main-sidebar sidebar-dark-primary elevation-4">
        <!-- Brand Logo -->
        <a href="#" class="brand-link">
            <?= Html::img('@web/AdminLTE/dist/img/AdminLTELogo.png', ['alt' => 'AdminLTE Logo', 'class' => 'brand-image img-circle elevation-3', 'style' => 'opacity: .8']) ?>

            <span class="brand-text font-weight-light"><strong>Z</strong>NEWS</span>
        </a>

        <!-- Sidebar -->
        <div class="sidebar">
            <!-- Sidebar user panel (optional) -->
            <div class="user-panel mt-3 pb-3 mb-3 d-flex">
                <div class="image">
                    <?php if (!empty(Yii::$app->user->identity->avatar) && file_exists('images/users/' . Yii::$app->user->identity->avatar)) { ?>
                        <?= Html::img('@web/images/users/' . Yii::$app->user->identity->avatar, ['alt' => 'admin', 'class' => 'img-circle elevation-2']) ?>
                    <?php } else { ?>
                        <?= Html::img('@web/images/no_avatar.png', ['alt' => 'admin', 'class' => 'img-circle elevation-2']) ?>
                    <?php } ?>
                </div>
                <div class="info">
                    <a href="#" class="d-block"><?= Yii::$app->user->identity->name . ' ' . Yii::$app->user->identity->surname ?> </a>
                </div>
            </div>

            <!-- Sidebar Menu -->
            <nav class="mt-2">
                <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">

                    <li class="nav-item">
                        <a href="<?= Url::toRoute('admin/category') ?>" class="nav-link <?= !empty($this->params['activePage']) && $this->params['activePage'] == 'category' ? 'active' : '' ?>">
                            <i class="nav-icon fas fa-list"></i>
                            <p>Categories</p>
                        </a>
                    </li>

                    <li class="nav-item">
                        <a href="<?= Url::toRoute('admin/news') ?>" class="nav-link <?= !empty($this->params['activePage']) && $this->params['activePage'] == 'news' ? 'active' : '' ?>">
                            <i class="nav-icon far fa-newspaper"></i>
                            <p>News</p>
                        </a>
                    </li>

                    <li class="nav-item">
                        <a href="<?= Url::toRoute('admin/contact') ?>"" class="nav-link <?= !empty($this->params['activePage']) && $this->params['activePage'] == 'contact' ? 'active' : '' ?>">
                            <i class="nav-icon fas fa-envelope"></i>
                            <p>Contact message</p>
                        </a>
                    </li>

                    <li class="nav-header">USER</li>

                    <li class="nav-item">
                        <a href="<?= Url::toRoute('admin/user') ?>" class="nav-link <?= !empty($this->params['activePage']) && $this->params['activePage'] == 'profile' ? 'active' : ''?>">
                            <i class="nav-icon far fa-user"></i>
                            <p>Profile</p>
                        </a>
                    </li>

                    <li class="nav-item">
                        <a class="nav-link" id="logout" data-text="Do you want to leave the admin-panel?" data-url="<?= Url::toRoute('auth/logout') ?>">
                            <i class="nav-icon fas fa-arrow-alt-circle-left"></i>
                            <p>Logout</p>
                        </a>
                    </li>
                </ul>
            </nav>
            <!-- /.sidebar-menu -->
        </div>
        <!-- /.sidebar -->
    </aside>


    <?=$content?>


    <!-- /.content-wrapper -->
    <footer class="main-footer">
        <strong>Copyright &copy; 2014-2019<a href="http://adminlte.io">AdminLTE.io</a>.</strong>
        All rights reserved.
        <div class="float-right d-none d-sm-inline-block">
            <b>Version</b> 3.0.0-rc.1
        </div>
    </footer>

    <!-- Control Sidebar -->
    <aside class="control-sidebar control-sidebar-dark">
        <!-- Control sidebar content goes here -->
    </aside>
    <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

<!-- jQuery UI 1.11.4 -->
<?php $this->registerJsFile('@web/AdminLTE/plugins/jquery-ui/jquery-ui.min.js') ?>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
    $.widget.bridge('uibutton', $.ui.button)
</script>
<!-- Bootstrap 4 -->
<?php $this->registerJsFile('@web/AdminLTE/plugins/bootstrap/js/bootstrap.bundle.min.js') ?>

<?php $this->registerJsFile('@web/AdminLTE/plugins/sweetalert2/sweetalert2.min.js') ?>

<?php $this->registerJsFile('@web/AdminLTE/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js') ?>
<!-- AdminLTE App -->
<?php $this->registerJsFile('@web/AdminLTE/dist/js/adminlte.js') ?>

<?php $this->registerJsFile('@web/plugins/Validation-Engine/jquery.validationEngine.js') ?>
<?php $this->registerJsFile('@web/plugins/Validation-Engine/jquery.validationEngine-en.js') ?>

<?php $this->registerJsFile('@web/AdminLTE/plugins/croppie/croppie.js') ?>
<?php $this->registerJsFile('@web/plugins/fancybox/dist/jquery.fancybox.min.js') ?>
<?php $this->registerJsFile('@web/js/admin-js.js') ?>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
