<?php

use yii\helpers\Html;
use yii\helpers\Url;

?>
<div class="content-wrapper" style="min-height: 1015.13px;">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>Profile</h1>
                    </div>

                </div>
            </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-md-6">
                    <!-- general form elements -->
                    <div class="card card-primary">
                        <div class="card-header">
                            <h3 class="card-title">Profile data</h3>
                        </div>
                        <!-- /.card-header -->
                        <!-- form start -->
                        <form id="users-form" action="<?= Url::toRoute(['admin/user/update-profile' , 'id' => $user['id']])?>" method="post" enctype="multipart/form-data">
                            <input type="hidden" name="_csrf" value="<?=Yii::$app->request->getCsrfToken()?>" />

                            <div class="card-body">
                                <div class="form-group">
                                    <label for="name">Name</label>
                                    <input type="text" class="form-control validate[required,minSize[3],maxSize[50]]" id="name" name="name" value="<?= !empty($user['name']) ? $user['name'] : '' ?>" placeholder="Name:">
                                </div>

                                <div class="form-group">
                                    <label for="surname">Surname</label>
                                    <input type="text" class="form-control validate[required,minSize[3],maxSize[50]]" id="surname" name="surname" value="<?= !empty($user['surname']) ? $user['surname'] : '' ?>" placeholder="Surname:">
                                </div>

                                <div class="form-group">
                                    <label for="login">Login</label>
                                    <input type="text" class="form-control validate[required,minSize[3],maxSize[50]]" id="login" name="login" value="<?= !empty($user['login']) ? $user['login'] : '' ?>" placeholder="Login:">
                                </div>

                                <div class="form-group">
                                    <label for="email">Email</label>
                                    <input type="text" class="form-control validate[required,custom[email]]" id="email" name="email" value="<?= !empty($user['email']) ? $user['email'] : '' ?>" placeholder="Email:">
                                </div>

                                <div class="form-group">
                                    <label for="user-avatar">Avatar</label>
                                    <input type="file" class="form-control" id="user-avatar">
                                </div>

                                <div id="avatar-cropper"></div>
                                <input type="hidden" id="cropped-image" name="avatar">

                                <?php if (!empty($user['avatar'])) { ?>
                                    <div id="old-avatar">
                                        <?= Html::img('@web/images/users/' . $user['avatar']) ?>
                                    </div>

                                    <input type="hidden" name="old_avatar" value="<?= $user['avatar'] ?>">

                                <?php } ?>
                            </div>
                            <!-- /.card-body -->

                            <div class="card-footer">
                                <button type="submit" class="btn btn-primary">Submit</button>
                            </div>
                        </form>
                    </div>
                    <!-- /.card -->

                </div>

                <div class="col-md-6">
                    <!-- general form elements -->
                    <div class="card card-danger">
                        <div class="card-header">
                            <h3 class="card-title">Change password</h3>
                        </div>
                        <!-- /.card-header -->

                        <!-- form start -->
                        <form id="change-password" action="<?= Url::toRoute(['admin/user/change-password' , 'id' => $user['id']])?>" method="post">
                            <input type="hidden" name="_csrf" value="<?=Yii::$app->request->getCsrfToken()?>" />

                            <div class="card-body">
                                <div class="form-group">
                                    <label for="old-password">Old password</label>
                                    <input type="password" class="form-control validate[required,minSize[3]]" id="old-password" name="old_password" placeholder="Old password" autocomplete="off">
                                </div>

                                <div class="form-group">
                                    <label for="password">Password</label>
                                    <input type="password" class="form-control validate[required,minSize[3]]" id="password" name="password" placeholder="Password" autocomplete="off">
                                </div>

                            </div>
                            <!-- /.card-body -->

                            <div class="card-footer">
                                <button type="submit" class="btn btn-primary">Change</button>
                            </div>
                        </form>
                    </div>
                    <!-- /.card -->

                </div>

            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>

    <script>
        $(document).ready(function() {

            var validation = $("#users-form").validationEngine('attach',{promptPosition : "bottomLeft", maxErrorsPerField: 1, validateNonVisibleFields: true, updatePromptsPosition:true});
            var validationPassword = $("#change-password").validationEngine('attach',{promptPosition : "bottomLeft", maxErrorsPerField: 1, validateNonVisibleFields: true, updatePromptsPosition:true});
            var croppie;

            //Init croppie
            $('#user-avatar').on('change', function(){

                var allowExtansion = ['image/jpeg', 'image/jpg', 'image/png'];
                if (!allowExtansion.includes(this.files[0].type)) {
                    sweetalert('error', 'Oops...', 'The file extension is incorrect');
                    $('#user-avatar').val('');
                    return false;
                }

                if (typeof croppie != "undefined" && croppie != null) {
                    $('#avatar-cropper').croppie('destroy');
                }

                $('#old-avatar').hide();

                croppie = $('#avatar-cropper').croppie({
                    enableExif: true,

                    viewport: {
                        width: 250,
                        height: 250,
                        type: 'square'
                    },
                    boundary: {
                        width: 300,
                        height: 300
                    }
                });

                if (this.files && this.files[0]) {
                    var reader = new FileReader();

                    reader.onload = function (e) {
                        croppie.croppie('bind', {
                            url: e.target.result
                        }).then(function(){});
                    }

                    reader.readAsDataURL(this.files[0]);
                    $('.js-confirm-crop').prop("disabled", false);
                }
            });

            //submit event profali data form
            $('#users-form').on('submit', function (e) {
                e.preventDefault();

                if (typeof croppie != 'undefined' && typeof croppie != null) {
                    croppie.croppie('result', {
                        type: 'base64',
                        size: 'viewport',
                        format: 'jpeg',
                        quality: 0.8,
                        backgroundColor: "#fff"

                    }).then(function (resp){
                        $('#cropped-image').val(resp);
                        profileDataAjax();
                    });

                } else {
                    profileDataAjax();
                }
            });

            //function for send profile data Ajax
            function profileDataAjax() {
                if (!validation.validationEngine('validate')) {
                    return false;
                }

                $.ajax({
                    url :$('#users-form').attr('action'),
                    type : 'POST',
                    dataType : 'JSON',
                    data : $('#users-form').serialize(),
                    success : function(response){
                        sweetalert('success', 'Success', response.message);

                    },
                    error : function (response) {
                        let errors = response.responseJSON.errors;
                        let firstKey = Object.keys(errors)[0];

                        sweetalert('error', 'Oops...', errors[firstKey][0]);
                    }
                });
            }

            $('#change-password').on('submit', function (e) {
                e.preventDefault();

                if (!validationPassword.validationEngine('validate')) {
                    return false;
                }

                $.ajax({
                    url :$('#change-password').attr('action'),
                    type : 'POST',
                    dataType : 'JSON',
                    data : $('#change-password').serialize(),
                    success : function(response) {
                        sweetalert('success', 'Success', response.message);
                        $('#change-password')[0].reset();
                    },
                    error : function (response) {
                        let errors = response.responseJSON;
                        sweetalert(errors.status, 'Oops...', errors.errors);
                    }
                });
            })
        });

    </script>