<?php

use yii\helpers\Url;

?>
<button type="button" class="btn btn-danger confirm" data-text="Do you want delete this record?" data-url="<?= Url::toRoute(['admin/contact/delete', 'id' => $id_contact]) ?>">
    <i class="fas fa-trash-alt"></i> Delete
</button>