<?php

use yii\helpers\Url;

?>
<input class="change-flag" id="must-read-<?= $id_contact ?>" data-url="<?= Url::toRoute(['admin/contact/is-viewed', 'id' => $id_contact]) ?>" type="checkbox"  <?= $viewed ? "checked" : " " ?>>
<label class="switch-color" for="must-read-<?= $id_contact ?>"></label>