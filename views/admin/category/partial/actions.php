<?php

use yii\helpers\Url;

?>
<a data-fancybox data-type="ajax" data-src="<?=Url::toRoute(['admin/category/form', 'id' =>  $id_cat])?>" href="javascript:;" class="btn btn-primary"><i class="fas fa-edit"></i> Edit</a>
<button type="button" class="btn btn-danger confirm" data-text="Do you want delete this category?" data-url="<?=Url::toRoute(['admin/category/delete', 'id' => $id_cat])?>"><i class="fas fa-trash-alt"></i> Delete</button>