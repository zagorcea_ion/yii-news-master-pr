<?php

use yii\helpers\Url;

?>
<div class="content-wrapper" style="min-height: 1015.13px;">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Categories</h1>
                </div>

            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">List of all categories</h3>

                        <div class="card-tools">
                            <a data-fancybox data-type="ajax" data-src="<?=Url::toRoute('admin/category/form')?>" href="javascript:;" class="btn btn-success"><i class="fas fa-plus"></i> Add category</a>
                        </div>

                        <?php if (Yii::$app->session->has('success') && Yii::$app->session->has('message')) { ?>
                            <script>
                                $(document).ready(function() {
                                    sweetalert("<?=Yii::$app->session->getFlash('success')?>", "Success", "<?=Yii::$app->session->getFlash('message')?>");
                                })
                            </script>
                        <?php }?>

                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <div id="example2_wrapper" class="dataTables_wrapper dt-bootstrap4"><div class="row"><div class="col-sm-12 col-md-6"></div><div class="col-sm-12 col-md-6"></div></div><div class="row"><div class="col-sm-12">
                                    <table id="categories-table" class="table table-bordered table-hover dataTable" role="grid" aria-describedby="example2_info">
                                        <thead>
                                        <tr role="row">
                                            <th>Name</th>
                                            <th>Date add</th>
                                            <th>Actions</th>
                                        </tr>
                                        </thead>
                                        <tbody>

                                        </tbody>

                                    </table>
                                </div>
                                <!-- /.card-body -->
                            </div>
                            <!-- /.card -->

                            <!-- /.card -->
                        </div>
                        <!-- /.col -->
                    </div>
                    <!-- /.row -->
    </section>
    <!-- /.content -->
</div>

<script>
    var dt;

    $(document).ready(function() {

        dt = $('#categories-table').DataTable( {
            "processing": true,
            "serverSide": true,
            "searching": true,
            "autoWidth": false,
            responsive: true,
            stateSave: true,
            "order": [ 1, 'desc' ],
            "columnDefs": [
                {"orderable": true, "targets": 0},
                {"orderable": true, className: "ta-c w-100", "targets": 1},
                {"orderable": false, className: "ta-c w-200", "targets": 2},
            ],
            "columns": [
                { "data": "name"},
                { "data": "date_add" },
                { "data": "action" },
            ],
            "ajax": function (data, callback, settings) {
                $.ajax({
                    "dataType": 'JSON',
                    "type": "POST",
                    "data": data,
                    url: "<?= Url::toRoute('admin/category/category-list') ?>",
                    "success": function (data, textStatus, jqXHR) {
                        callback(data, textStatus, jqXHR);
                    }
                });
            },

        });
    });
</script>
