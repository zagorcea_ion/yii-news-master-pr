<?php

use yii\helpers\Url;

?>

<div class="card card-primary modal-form" style="padding: 0!important; width: 400px">
    <div class="card-header" style="border-radius: 0!important;">
        <h3 class="card-title"><?=!empty($category['id_cat']) ? 'Edit' : 'Add' ?> category</h3>
    </div>

    <form id="category-form" action="<?=!empty($category['id_cat']) ? Url::toRoute(['admin/category/update' , 'id' => $category['id_cat']]) : Url::toRoute('admin/category/create')?>" method="post">

        <?php if (!empty($category['id_cat'])) { ?>
            <input type="hidden" name="id_cat" value="<?=$category['id_cat']?>">
        <?php } ?>

        <input type="hidden" name="_csrf" value="<?=Yii::$app->request->getCsrfToken()?>" />
        <div class="card-body">
            <div class="form-group">
                <label for="name">Category name</label>
                <input type="text" class="form-control validate[required]" id="name" name="name" value="<?=!empty($category['name']) ? $category['name'] : ''?>" placeholder="Category name:">
            </div>
        </div>

        <div class="card-footer">
            <button type="submit" class="btn btn-primary">Submit</button>
        </div>
    </form>
</div>


<script>
    var validation = $("#category-form").validationEngine('attach',{promptPosition : "bottomLeft", maxErrorsPerField: 1});

    $('#category-form').on('submit', function (e) {
        e.preventDefault();
        if(!validation.validationEngine('validate')){
            return false;
        }

        $.ajax({
            url : $(this).attr('action'),
            type : 'POST',
            dataType : 'JSON',
            data : $(this).serialize(),
            success : function(response){
                sweetalert(response.status, 'Success', response.message);
                $.fancybox.close();
                dt.draw(false);
            },

            error : function (response) {
                let errors = response.responseJSON.errors;
                let firstKey = Object.keys(errors)[0];

                sweetalert('error', 'Oops...', errors[firstKey][0]);
            }
        });
    });

</script>