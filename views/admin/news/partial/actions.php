<?php

use yii\helpers\Url;

?>
<a href="<?=Url::toRoute(['admin/news/form', 'id' =>  $id_news])?>" class="btn btn-primary">
    <i class="fas fa-edit"></i> Edit
</a>

<button type="button" class="btn btn-danger confirm" data-text="Do you want delete this news?" data-url="<?=Url::toRoute(['admin/news/delete', 'id' => $id_news])?>">
    <i class="fas fa-trash-alt"></i> Delete
</button>