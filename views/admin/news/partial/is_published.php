<?php

use yii\helpers\Url;

?>
<input class="change-flag" id="published-<?=$id_news?>" data-url="<?=Url::toRoute(['admin/news/is-published', 'id' => $id_news])?>" type="checkbox" <?=$is_published ? "checked" : " "?>>
<label class="switch-color" for="published-<?=$id_news?>"></label>