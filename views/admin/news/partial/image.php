
<?php

use yii\helpers\Html;

if (isset($image) &&!empty($image)) { 
    echo Html::img('@web/images/news/' . $image, ['alt' => $title, 'width' => '100px']);
    
} else { 
    echo Html::img('@web/images/no_avatar.png', ['alt' => $title, 'width' => '100px']);
} 


