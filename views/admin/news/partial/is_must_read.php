<?php

use yii\helpers\Url;

?>
<input class="change-flag" id="must-read-<?=$id_news?>" data-url="<?=Url::toRoute(['admin/news/is-must-read', 'id' => $id_news])?>" type="checkbox"  <?=$is_must_read ? "checked" : " "?>>
<label class="switch-color" for="must-read-<?=$id_news?>"></label>
