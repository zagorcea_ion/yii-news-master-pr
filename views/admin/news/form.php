<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

?>

<?php $this->registerJsFile('@web/plugins/tinymce/tinymce.min.js') ?>


<div class="content-wrapper" style="min-height: 1015.13px;">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>News</h1>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <!-- left column -->
                <div class="col-md-12">
                    <!-- general form elements -->
                    <div class="card card-primary">
                        <div class="card-header">
                            <h3 class="card-title">Add news</h3>
                        </div>

                        <?php $form = ActiveForm::begin([ 'id' => 'news-form', 'action' => !empty($news['id_news']) ? ['admin/news/update', 'id' => $news['id_news']] : ['admin/news/create'], 'options' => ['enctype' => 'multipart/form-data']]) ?>

                            <div class="card-body">
                                <div class="form-group">
                                    <?= $form->field($model, 'title', ['inputOptions' => ['class' => 'form-control validate[required]']])
                                            ->textInput(['placeholder' => "Title:", 'value' => !empty($news['title']) ? $news['title'] : ''])?>
                                </div>

                                <div class="form-group">
                                    <?= $form->field($model, 'description', ['inputOptions' => ['class' => 'form-control validate[required]']])
                                        ->textarea(['rows' => '3', 'placeholder' => "Description:", 'value' => !empty($news['description']) ? $news['description'] : '']) ?>
                                </div>

                                <div class="form-group">
                                    <label for="id-category">Category</label>
                                    <select class="form-control validate[required]" id="id-category" name="News[id_category]">
                                        <option selected disabled>Select category</option>

                                        <?php  if (!empty($categories)) { ?>
                                            <?php foreach ($categories as $category) { ?>
                                                <option value="<?=$category['id_cat']?>" <?=!empty($news['id_category']) && $news['id_category'] == $category['id_cat'] ? 'selected' : ''?>>
                                                    <?=$category['name']?>
                                                </option>
                                            <?php } ?>
                                        <?php } ?>

                                    </select>
                                </div>

                                <div class="form-group">
                                    <label for="date-publish">Date publish</label>
                                    <input type="date" class="form-control validate[required]" id="date-publish" name="News[date_publish]" value="<?=!empty($news['date_publish']) ? date('Y-m-d', strtotime($news['date_publish'])) : ''?>">
                                </div>

                                <div class="form-group">
                                    <?= $form->field($model, 'content', ['inputOptions' => ['id' => 'content', 'class' => 'form-control validate[required]']])
                                            ->textarea(['placeholder' => "Content:", 'value' => !empty($news['content']) ? $news['content'] : '']) ?>
                                </div>

                                <?php if (!empty($news['image'])) { ?>
                                    <input type="hidden" name="News[old_image]" value="<?=$news['image']?>">
                                    <div class="form-group">
                                        <?= Html::img('@web/images/news/' . $news['image'], ['alt' => $news['title'], 'width' => '200px'])?>
                                    </div>
                                <?php } ?>

                                <div class="form-group">
                                    <?= $form->field($model, 'image', ['inputOptions' => ['class' => 'form-control validate[required]']])->fileInput() ?>
                                </div>

                                <div class="form-group">
                                    <label>Published</label>
                                    <input id="published" name="News[is_published]" type="checkbox" <?=!empty($news['is_published']) && $news['is_published'] ?  'checked'  : ''?>>
                                    <label class="switch-color" for="published"></label>
                                </div>

                                <div class="form-group">
                                    <label>Must read</label>
                                    <input id="must-read" name="News[is_must_read]" type="checkbox" <?=!empty($news['is_must_read']) && $news['is_must_read'] ?  'checked'  : ''?>>
                                    <label class="switch-color" for="must-read"></label>
                                </div>

                                <div class="card-footer">
                                    <button type="submit" class="btn btn-primary">Submit</button>
                                </div>

                            </div>

                        <?php ActiveForm::end() ?>

                    </div>
                    <!-- /.card -->
                </div>
                <!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
    </section>
    <!-- /.content -->
</div>

<script>
    $(document).ready(function() {
        tinymce.init({
            selector: '#content',
            entity_encoding: 'raw',
            menubar: false,
            branding: false,
            height: 500,
            max_height: 700,
            min_height: 500,
            plugins: [
                'autolink autoresize fullscreen link lists paste',
            ],
            toolbar: 'undo redo | bold italic | underline strikethrough | bullist numlist | link | fullscreen',

            link_assume_external_targets: true,
            relative_urls: false,
            image_advtab: true ,
            remove_script_host: false,
            force_br_newlines: false,
            force_p_newlines: false,
            forced_root_block: "",
            extended_valid_elements: "br",
            verify_html: false,
            valid_children: "br",
            paste_as_text: true,

        });

        // var validation = $("#news-form").validationEngine('attach',{promptPosition : "bottomLeft", maxErrorsPerField: 1, validateNonVisibleFields: true, updatePromptsPosition:true});

        $('#news-form').on('submit', function (e) {
            e.preventDefault();
            e.stopImmediatePropagation();
            // if(!validation.validationEngine('validate')){
            //     return false;
            // }

            var formData = new FormData(this);

            $.ajax({
                url : $(this).attr('action'),
                type : 'POST',
                dataType : 'JSON',
                cache: false,
                contentType: false,
                processData: false,
                data : formData,
                success : function(response){
                    if (typeof response.redirect != 'undefined' && typeof response.redirect != null) {
                        window.location.replace(response.redirect);
                    }
                },
                error : function (response) {
                    let errors = response.responseJSON.errors;
                    let firstKey = Object.keys(errors)[0];

                    sweetalert('error', 'Oops...', errors[firstKey][0]);
                }
            });

            return false;
        });
    });

</script>
