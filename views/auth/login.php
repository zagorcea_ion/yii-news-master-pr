<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

?>

<?php $this->beginPage() ?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">

        <title>Admin Znews | Log in</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?= Html::csrfMetaTags() ?>

        <link rel="shortcut icon" type="image/x-icon" href="images/favicon.ico">
        <?php $this->head() ?>

        <!-- Font Awesome -->
        <?php $this->registerCssFile('@web/AdminLTE/plugins/fontawesome-free/css/all.min.css')?>

        <!-- Ionicons -->
        <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
        <!-- icheck bootstrap -->
        <?php $this->registerCssFile('@web/AdminLTE/plugins/icheck-bootstrap/icheck-bootstrap.min.css')?>
        
        <!-- Theme style -->
        <?php $this->registerCssFile('@web/AdminLTE/dist/css/adminlte.min.css')?>



        <?php $this->registerCssFile('@web/AdminLTE/plugins/sweetalert2-theme-bootstrap-4/bootstrap-4.min.css')?>

        <!-- Google Font: Source Sans Pro -->
        <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
    </head>
    <body class="hold-transition login-page">
        <?php $this->beginBody() ?>
        <div class="login-box">
            <div class="login-logo">
                <a href="#"><b>Z</b>NEWS</a>
            </div>
            <!-- /.login-logo -->
            <div class="card">
                <div class="card-body login-card-body">
                    <p class="login-box-msg">Sign in to start your session</p>

                    <?php $form = ActiveForm::begin([ 'id' => 'user-login', 'options' => ['method' => 'post']]) ?>

                        <?= $form->field($model, 'username', ['inputOptions' => ['class' => 'form-control']])->textInput(['placeholder' => "Login"])?>
                        <?= $form->field($model, 'password')->passwordInput(['placeholder' => "Password"])?>
                     
                        <div class="row">

                            <!-- /.col -->
                            <div class="col-4">
                                <button type="submit" class="btn btn-primary btn-block btn-flat">Sign In</button>
                            </div>
                            <!-- /.col -->
                        </div>
                    <?php ActiveForm::end() ?>
                </div>
                <!-- /.login-card-body -->
            </div>
        </div>
        <!-- /.login-box -->

        <!-- jQuery -->
        <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>

        <!-- Bootstrap 4 -->
        <?php $this->registerJsFile('@web/AdminLTE/plugins/bootstrap/js/bootstrap.bundle.min.js')?>

        <?php $this->registerJsFile('@web/AdminLTE/plugins/sweetalert2/sweetalert2.min.js')?>

        <?php $this->registerJsFile('@web/js/admin-js.js')?>

        <!-- <script>
            $(document).ready(function() {
                $('#user-login').on('submit', function (e) {
                    e.preventDefault();

                    $.ajax({
                        url :$('#user-login').attr('action'),
                        type : 'POST',
                        dataType : 'JSON',
                        data : $('#user-login').serialize(),
                        success : function(response){
                            window.location.replace("{{route('news.index')}}");
                        },
                        error: function (error) {
                            onSaveRequestError(error);
                        }
                    });
                })
            })
        </script> -->
        <?php $this->endBody() ?>

    </body>
</html>
<?php $this->endPage() ?>
