<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "contact".
 *
 * @property int $id_contact
 * @property string $email
 * @property string $name
 * @property string|null $phone
 * @property string|null $website
 * @property string $message
 * @property int|null $viewed
 * @property string|null $created_at
 */
class Contact extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'contact';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['email', 'name', 'message'], 'required'],
            [['viewed'], 'integer'],
            [['created_at'], 'safe'],
            [['email'], 'string', 'max' => 100],
            [['name', 'phone', 'website'], 'string', 'max' => 50],
            [['message'], 'string', 'max' => 500],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_contact' => 'Id Contact',
            'email'      => 'Email',
            'name'       => 'Name',
            'phone'      => 'Phone',
            'website'    => 'Website',
            'message'    => 'Message',
            'viewed'     => 'Viewed',
            'created_at' => 'Created At',
        ];
    }
}
