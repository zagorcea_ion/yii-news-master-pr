<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;

/**
 * This is the model class for table "news".
 *
 * @property int $id_news
 * @property int $id_category
 * @property string|null $title
 * @property string|null $alias
 * @property string|null $description
 * @property string|null $content
 * @property string|null $image
 * @property int|null $views
 * @property int|null $is_published
 * @property int|null $is_must_read
 * @property string|null $date_publish
 * @property string|null $created_at
 */
class Users extends ActiveRecord implements IdentityInterface
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'users';
    }

     /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'surname', 'login', 'email'], 'required'],
            [['name', 'surname', 'login'], 'string', 'max' => 50],
            [['email'], 'string', 'max' => 100],
            [['login', 'email'], 'unique'],
            [['avatar'], 'safe']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'name'    => 'Name',
            'surname' => 'Surname',
            'login'   => 'Login',
            'email'   => 'Email',
        ];
    }

    /**
     * {@inheritdoc}
     */
    public static function findIdentity($id)
    {
        return Users::findOne($id);
    }

     /**
     * {@inheritdoc}
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        foreach (self::$users as $user) {
            if ($user['accessToken'] === $token) {
                return new static($user);
            }
        }

        return null;
    }

    
    /**
     * {@inheritdoc}
     */
    public function getId()
    {
        return $this->id;
    }


        /**
     * {@inheritdoc}
     */
    public function getAuthKey()
    {
        return $this->authKey;
    }

    /**
     * {@inheritdoc}
     */
    public function validateAuthKey($authKey)
    {
        return $this->authKey === $authKey;
    }

    public static function findByUsername($username) 
    {
        return Users::find()->where(['login' => $username])->one();
    }
   
    public function validatePassword($password)
    {
        return (Yii::$app->getSecurity()->validatePassword($password, $this->password)) ? true : false;
    }

}