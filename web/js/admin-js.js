var sweetalert = function (type, title, message) {
    Swal.fire({
        type: type,
        title: title,
        text: message,
    })

}

//Confirm event
$('body').on('click', '.confirm', function () {
    Swal.fire({
        title: 'Are you sure?',
        text: $(this).data('text'),
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes'
    }).then((result) => {
        if (result.value) {
            $.ajax({
                url : $(this).data('url'),
                type : 'POST',
                dataType : 'JSON',
                success : function(response){
                    if(response.status == 'success'){
                        sweetalert(response.status, 'Success', response.message);

                        if (typeof dt != null && typeof dt != 'undefined') {
                            dt.draw(false);
                        }
                    }

                    if(response.status == 'error'){
                        sweetalert(response.status, 'Oops...', response.message);
                    }
                }
            });
        }
    })

});

//Change flag event
$('body').on('change', '.change-flag', function () {

	var url = $(this).data('url');
	console.log(url);
	$.ajax({
		dataType: 'JSON',
		method: 'POST',
		url: $(this).data('url'),

		success : function(response){
			if(response.status == 'success'){
				sweetalert(response.status, 'Success', response.message);
			}

			if(response.status == 'error'){
				sweetalert(response.status, 'Oops...', response.message);
			}

			if (typeof dt != null && typeof dt != 'undefined') {
				dt.draw(false);
			}
		}

	});
})

//Logout event
$('body').on('click', '#logout', function () {
	Swal.fire({
		title: 'Are you sure?',
		text: $(this).data('text'),
		type: 'warning',
		showCancelButton: true,
		confirmButtonColor: '#3085d6',
		cancelButtonColor: '#d33',
		confirmButtonText: 'Yes'
	}).then((result) => {
		if (result.value) {
			window.location.replace($(this).data('url'));
		}
	})
});

(function(window, $) {
    let csrf_token = document.head.querySelector('meta[name="csrf-token"]');
    // let csrf_param = document.head.querySelector('meta[name="csrf-param"]');

    // csrf_param = csrf_param.content;
    csrf_token = csrf_token.content;


    if (csrf_token) {
        $.ajaxSetup({
            data: {'_csrf': csrf_token}
        })
    } else {
        console.error('CSRF token not found');
    }

} (window, jQuery));



