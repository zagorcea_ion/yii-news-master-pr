<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%contact}}`.
 */
class m200404_175039_create_contact_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%contact}}', [
            'id_contact' => $this->primaryKey(),
            'email'      => $this->string(100)->notNull(),
            'name'       => $this->string(50)->notNull(),
            'phone'      => $this->string(50),
            'website'    => $this->string(50),
            'message'    => $this->string(500)->notNull(),
            'viewed'     => $this->integer(1)->defaultValue(0),
            'created_at' => $this->dateTime(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%contact}}');
    }
}
