<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%users}}`.
 */
class m200330_084340_create_users_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%users}}', [
            'id'       => $this->primaryKey(),
            'name'     => $this->string(50)->notNull(),
            'surname'  => $this->string(50)->notNull(),
            'login'    => $this->string(50)->notNull()->unique(),
            'email'    => $this->string(100)->notNull()->unique(),
            'is_admin' => $this->integer(1)->defaultValue(0),
            'password' => $this->string(255)->notNull(),
            'avatar'   => $this->string(255),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%users}}');
    }
}
