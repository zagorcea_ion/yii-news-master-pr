<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%news}}`.
 */
class m200312_154757_create_news_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%news}}', [
            'id_news'      => $this->primaryKey(),
            'id_category'  => $this->integer()->notNull(),
            'title'        => $this->string('255'),
            'alias'        => $this->string('255'),
            'description'  => $this->string('255'),
            'content'      => $this->text(),
            'image'        => $this->string('255'),
            'views'        => $this->integer()->defaultValue(0),
            'is_published' => $this->integer(1)->defaultValue(0),
            'is_must_read' => $this->integer(1)->defaultValue(0),
            'date_publish' => $this->dateTime(),
            'created_at'   => $this->dateTime(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%news}}');
    }
}
