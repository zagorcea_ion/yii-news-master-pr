<?php


namespace app\controllers;

use app\helpers\MyUrl;
use app\models\Contact;
use app\models\News;
use Yii;
use yii\data\Pagination;
use yii\db\Expression;
use yii\web\Controller;

class PageController  extends Controller
{

    /**
     * Show Home page.
     * @return mixed
     */
    public function actionIndex()
    {
        $data = [
            'news'         => News::find()->with('category')->where(['is_published' => 1])->orderBy(['date_publish' => SORT_DESC])->limit(10)->all(),
            'most_popular' => News::find()->where(['is_published' => 1])->orderBy(['views' => SORT_DESC])->limit(6)->all(),
            'must_read'    => News::find()->with('category')->where(['is_must_read' => 1])->where(['is_published' => 1])->orderBy(['date_publish' => SORT_DESC])->limit(6)->all()
        ];

        return $this->render('index', $data);
    }

    /**
     * Show News page.
     * @param string $category
     * @return mixed
     */
    public function actionNews($category = '')
    {
        if (!empty($category)) {
            $idCategory = MyUrl::getIdUrl($category);
            $news = News::find()->with('category')->where(['is_published' => 1])->where(['id_category' => $idCategory]);

        } else {
            $news = News::find()->with('category')->where(['is_published' => 1]);
        }

        $pagination = new Pagination([
           'defaultPageSize' => 10,
           'totalCount' => $news->count()
        ]);

        $news = $news->orderBy(['date_publish' => SORT_DESC])->offset($pagination->offset)->limit($pagination->limit)->all();

        $data = [
            'news'         => $news,
            'pagination'   => $pagination,
            'most_popular' => News::find()->where(['is_published' => 1])->orderBy(['views' => SORT_DESC])->limit(6)->all(),
        ];
        return $this->render('news', $data);
    }

    /**
     * Show News detail page.
     * @param $alias
     * @return mixed
     */
    public function actionNewsDetail($alias)
    {
        $id = MyUrl::getIdUrl($alias);

        $news = News::findOne($id);
        $news->views += 1;
        $news->save();

        $data = [
            'news'               => $news,
            'most_popular'       => News::find()->where(['is_published' => 1])->orderBy(['views' => SORT_DESC])->limit(6)->all(),
            'can_be_interesting' => News::find()->where(['!=', 'id_news', $id])->orderBy(new Expression('rand()'))->limit(3)->all(),
        ];

        return $this->render('news_detail', $data);

    }

    /**
     * Show Contact page.
     * @return mixed
     */
    public function actionContact()
    {
        return $this->render('contact');
    }

    /**
     * Save contact message.
     * @return mixed
     */
    public function actionSaveContact()
    {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        $contact = new Contact();
        $contact->name = Yii::$app->request->post('name');
        $contact->email = Yii::$app->request->post('email');
        $contact->phone = Yii::$app->request->post('phone');
        $contact->website = Yii::$app->request->post('website');
        $contact->message = Yii::$app->request->post('message');
        $contact->created_at = date('Y-m-d H:i:s');

        if (!$contact->save()) {
            if ($contact->hasErrors()) {
                Yii::$app->response->statusCode = 422;

                return [
                    'status' => 'error',
                    'errors' => $contact->getErrors(),
                ];
            }

        } else {
            Yii::$app->response->statusCode = 200;

            return [
                'status'  => 'success',
                'message' => "Contact message was added successfully",
            ];
        }
    }

    /**
     * Show Error page.
     * @return mixed
     */
    public function actionError()
    {
        return $this->render('error');
    }
}