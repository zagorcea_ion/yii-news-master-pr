<?php

namespace app\controllers\admin;

use app\helpers\MyUrl;
use app\models\Categories;
use Yii;
use app\models\News;
use yii\db\StaleObjectException;
use yii\helpers\Url;
use yii\web\UploadedFile;

/**
 * NewsController implements the CRUD actions for News model.
 */
class NewsController extends AdminAppController
{
    public $layout = 'admin';

    /**
     * Lists all News models.
     * @return mixed
     */
    public function actionIndex()
    {
        $this->view->params['activePage'] = 'news';

        return $this->render('index');
    }

    /**
     * Lists all News models.
     * @return mixed
     */
    public function actionNewsList()
    {
        $this->layout = false;

        $columns = [
            0 => 'title',
            5 => 'created_at',
        ];

        //Config search data
        $search = Yii::$app->request->post('search');

        //Config order
        $orderConfig = Yii::$app->request->post('order');
        $orderColumn = $columns[$orderConfig[0]['column']];
        $orderDir = ($orderConfig[0]['dir'] == 'desc') ? SORT_DESC : SORT_ASC;

        //Build query
        $news = News::find()->with('category');

        if (!empty($search['value'])) {
            $news = $news->where(['like', 'title', $search['value']]);
        }

        $recordsFilteredCount = $news->count();     //count filtered records
        $news = $news->asArray()
            ->orderBy([$orderColumn => $orderDir])
            ->limit(Yii::$app->request->post('length'))
            ->offset(Yii::$app->request->post('start'))
            ->all();

        $data = [];

        if (!empty($news)) {
            foreach ($news as $key => $item) {
                $data[$key] = [
                    'title'        => $item['title'],
                    'category'     => $item['category']['name'],
                    'image'        => $this->render('partial/image', $item),
                    'is_published' => $this->render('partial/is_published', $item),
                    'is_must_read' => $this->render('partial/is_must_read', $item),
                    'date_add'     => date('d.m.Y', strtotime($item['created_at'])),
                    'actions'      => $this->render('partial/actions', $item)
                ];
            }
        }

        $output['data'] = $data;
        $output['recordsTotal'] = News::find()->count();
        $output['recordsFiltered'] = $recordsFilteredCount;

        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        return $output;
    }



    /**
     * Show News model form.
     * @param null $id
     * @return mixed
     */
    public function actionForm($id = null)
    {
        $this->view->params['activePage'] = 'news';

        if (!is_null($id)) {
            $data['news'] = News::findOne($id);
        }

        $data['categories'] =  Categories::find()->asArray()->all();
        $data['model'] = new News();

        return $this->render('form', $data);
    }

    /**
     * Creates a new News model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        $postData = Yii::$app->request->post('News');
        $news = new News();

        $news->title = $postData['title'];
        $news->alias = MyUrl::makeSlugs($postData['title']);
        $news->description = $postData['description'];
        $news->content = $postData['content'];
        $news->id_category = $postData['id_category'];
        $news->is_published = is_null($postData['is_published']) ? 0 : 1;
        $news->is_must_read = is_null($postData['is_published']) ? 0 : 1;
        $news->date_publish = $postData['date_publish'];

        $news->created_at = date('Y-m-d H:i:s');

        if (!$news->save()) {
            if ($news->hasErrors()) {
                Yii::$app->response->statusCode = 422;

                return [
                    'status' => 'error',
                    'errors' => $news->getErrors(),
                ];
            }

        } else {

            Yii::$app->session->setFlash('success', "success");
            Yii::$app->session->setFlash('message', "News was added successfully");

            Yii::$app->response->statusCode = 200;

            return [
                'status'   => 'success',
                'message'  => "News was added successfully",
                'redirect' => Url::toRoute('admin/news/index'),
            ];
        }
    }

    /**
     * Updates an existing News model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        $postData = Yii::$app->request->post('News');
        
        $news = News::findOne($id);

        if (!is_null(UploadedFile::getInstance($news, 'image')) && !empty($postData['old_image'])) {
            unlink('images/news/' .  $postData['old_image']);
        }

        $news->title = $postData['title'];
        $news->alias = MyUrl::makeSlugs($postData['title']);
        $news->description = $postData['description'];
        $news->content = $postData['content'];
        $news->id_category = $postData['id_category'];
        $news->is_published = is_null($postData['is_published']) ? 0 : 1;
        $news->is_must_read = is_null($postData['is_published']) ? 0 : 1;
        $news->date_publish = $postData['date_publish'];

        $news->created_at = date('Y-m-d H:i:s');

        if (!$news->save()) {
            if ($news->hasErrors()) {
                Yii::$app->response->statusCode = 422;

                return [
                    'status' => 'error',
                    'errors' => $news->getErrors(),
                ];
            }

        } else {

            Yii::$app->session->setFlash('success', "success");
            Yii::$app->session->setFlash('message', "News was updated successfully");

            Yii::$app->response->statusCode = 200;

            return [
                'status'   => 'success',
                'message'  => "News was updated successfully",
                'redirect' => Url::toRoute('admin/news/index'),
            ];
        }
    }

    /**
     * Deletes an existing News model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws StaleObjectException
     * @throws \Throwable
     */
    public function actionDelete($id)
    {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        $news = News::findOne($id);

        if ($news->delete()) {
            Yii::$app->response->statusCode = 200;

            return [
                'status'  => 'success',
                'message' => "News was delete successfully",
            ];

        } else {
            Yii::$app->response->statusCode = 422;

            return [
                'status'  => 'error',
                'message' => "News was not delete successfully",
            ];
        }
    }

    /**
     * Make news published
     * @param int $id
     * @return array
     */
    public function actionIsPublished($id = 0)
    {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        $news = News::findOne($id);

        if (empty($news)) {
            return [
                'status'  => 'error',
                'message' => "News was not found",
            ];
        }

        if ($news->is_published) {
            $news->is_published = 0;
            $message = 'News was unpublished successfully';

        } else {
            $news->is_published = 1;
            $message = 'News was published successfully';
        }

        $news->save();

        return [
            'status'  => 'success',
            'message' => $message,
        ];
    }

    /**
     * Make news must read
     * @param int $id
     * @return array
     */
    public function actionIsMustRead($id = 0)
    {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        $news = News::findOne($id);

        if (empty($news)) {
            return [
                'status'  => 'error',
                'message' => "News was not found",
            ];
        }

        if ($news->is_must_read) {
            $news->is_must_read = 0;
            $message = 'News is not "must read"';

        } else {
            $news->is_must_read = 1;
            $message = 'News was made "must reade"';
        }

        $news->save();

        return [
            'status'  => 'success',
            'message' => $message,
        ];
    }
}
