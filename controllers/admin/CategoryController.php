<?php

namespace app\controllers\admin;

use app\helpers\MyUrl;
use Yii;
use app\models\Categories;
/**
 * CategoryController implements the CRUD actions for Categories model.
 */
class CategoryController extends AdminAppController
{
    public $layout = 'admin';

    /**
     * Lists all Categories models.
     * @return mixed
     */
    public function actionIndex()
    {
        $this->view->params['activePage'] = 'category';

        return $this->render('index');
    }

    /**
     * Lists all Categories models.
     * @return mixed
     */
    public function actionCategoryList()
    {
        $this->layout = false;

        $columns = [
            0 => 'name',
            1 => 'created_at',
        ];

        //Config search data
        $search = Yii::$app->request->post('search');

        //Config order
        $orderConfig = Yii::$app->request->post('order');
        $orderColumn = $columns[$orderConfig[0]['column']];
        $orderDir = ($orderConfig[0]['dir'] == 'desc') ? SORT_DESC : SORT_ASC;

        //Build query
        $category = Categories::find();

        if (!empty($search['value'])) {
            $category = $category->where(['like', 'name', $search['value']]);
        }

        $recordsFilteredCount = $category->count();     //count filtered records
        $category = $category->asArray()
            ->orderBy([$orderColumn => $orderDir])
            ->limit(Yii::$app->request->post('length'))
            ->offset(Yii::$app->request->post('start'))
            ->all();

        $data = [];

        if (!empty($category)) {
            foreach ($category as $key => $item) {
                $data[$key] = [
                    'name'     => $item['name'],
                    'date_add' => date('d.m.Y', strtotime($item['created_at'])),
                    'action'   => $this->render('partial/actions', $item)
                ];
            }
        }

        $output['data'] = $data;
        $output['recordsTotal'] = Categories::find()->count();
        $output['recordsFiltered'] = $recordsFilteredCount;

        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        return $output;
    }

    /**
     * Show Categories model form.
     * @param null $id
     * @return mixed
     */
    public function actionForm($id = null)
    {
        $this->layout = false;
        $data = [];

        if (!is_null($id)) {
            $data['category'] = Categories::findOne($id);
        }

        return $this->render('form', $data);
    }


    /**
     * Creates a new Categories model.
     * @return mixed
     */
    public function actionCreate()
    {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        $model = new Categories();

        $model->name = Yii::$app->request->post('name');
        $model->alias = MyUrl::makeSlugs(Yii::$app->request->post('name'));
        $model->created_at = date('Y-m-d H:i:s');

        if (!$model->save()) {
            if ($model->hasErrors()) {
                Yii::$app->response->statusCode = 422;

                return [
                    'status' => 'error',
                    'errors' => $model->getErrors(),
                ];
            }

        } else {
            Yii::$app->response->statusCode = 200;

            return [
                'status'  => 'success',
                'message' => "Category was added successfully",
            ];
        }
    }

    /**
     * Updates an existing Categories model.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        $model = Categories::findOne($id);

        $model->name = Yii::$app->request->post('name');
        $model->alias = MyUrl::makeSlugs(Yii::$app->request->post('name'));
        $model->created_at = date('Y-m-d H:i:s');

        if (!$model->save()) {
            if ($model->hasErrors()) {
                Yii::$app->response->statusCode = 422;

                return [
                    'status' => 'error',
                    'errors' => $model->getErrors(),
                ];
            }

        } else {
            Yii::$app->response->statusCode = 200;

            return [
                'status'  => 'success',
                'message' => "Category was updated successfully",
            ];
        }
    }

    /**
     * Deletes an existing Categories model.
     * @param integer $id
     * @return mixed
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionDelete($id)
    {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        $category = Categories::findOne($id);

        if ($category->delete()) {
            Yii::$app->response->statusCode = 200;

            return [
                'status'  => 'success',
                'message' => "Category was delete successfully",
            ];

        } else {
            Yii::$app->response->statusCode = 422;

            return [
                'status'  => 'error',
                'message' => "Category was not delete successfully",
            ];
        }
    }
}
