<?php

namespace app\controllers\admin;

use app\helpers\MyImage;

use app\models\Users;
use Yii;
use yii\base\Exception;

/**
 * UserController implements the CRUD actions for User model.
 */
class UserController extends AdminAppController
{
    public $layout = 'admin';

    /**
     * User profile page.
     * @return mixed
     */
    public function actionIndex()
    {
        $this->view->params['activePage'] = 'profile';

        $data['user'] = Users::findOne(Yii::$app->user->identity->id);

        return $this->render('profile', $data);
    }

    /**
     * User profile page.
     * @param $id
     * @return mixed
     */
    public function actionUpdateProfile($id)
    {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        $user = Users::findOne($id);

        $user->name = Yii::$app->request->post('name');
        $user->surname = Yii::$app->request->post('surname');
        $user->login = Yii::$app->request->post('login');
        $user->email = Yii::$app->request->post('email');

        if (!empty(Yii::$app->request->post('avatar'))) {
            $user->avatar = MyImage::base64Save(Yii::$app->request->post('avatar'), 'images/users/');
        }

        if (!$user->save()) {
            if ($user->hasErrors()) {
                Yii::$app->response->statusCode = 422;

                return [
                    'status' => 'error',
                    'errors' => $user->getErrors(),
                ];
            }

        } else {
            Yii::$app->response->statusCode = 200;

            if (!empty(Yii::$app->request->post('avatar')) && !empty(Yii::$app->request->post('old_avatar')) && file_exists('images/users/' . Yii::$app->request->post('old_avatar'))) {
                unlink('images/users/' . Yii::$app->request->post('old_avatar'));
            }

            return [
                'status' => 'success',
                'message' => "Profile was updated successfully",
            ];
        }
    }

    /**
     * User change password.
     * @param $id
     * @return mixed
     * @throws Exception
     */
    public function actionChangePassword($id)
    {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        $user = Users::findOne($id);

        if (!Yii::$app->getSecurity()->validatePassword(Yii::$app->request->post('old_password'), $user->password)) {
            Yii::$app->response->statusCode = 422;

            return [
                'status' => 'error',
                'errors' => 'Old password is incorrect',
            ];
        }

        $user->password = Yii::$app->getSecurity()->generatePasswordHash(Yii::$app->request->post('password'));

        if ($user->save()) {
            Yii::$app->response->statusCode = 200;

            return [
                'status'   => 'success',
                'message'  => "Password was added changed",
            ];
        }
    }
}