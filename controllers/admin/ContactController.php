<?php

namespace app\controllers\admin;

use app\models\Contact;
use Yii;
use yii\db\StaleObjectException;

/**
 * ContactController implements the CRUD actions for Contact model.
 */
class ContactController extends AdminAppController
{
    public $layout = 'admin';

    /**
     * Lists all Categories models.
     * @return mixed
     */
    public function actionIndex()
    {
        $this->view->params['activePage'] = 'contact';

        return $this->render('index');
    }

    /**
     * Lists all Contact models.
     * @return mixed
     */
    public function actionContactList()
    {
        $this->layout = false;

        $columns = [
            5 => 'created_at',
        ];

        //Config order
        $orderConfig = Yii::$app->request->post('order');
        $orderColumn = $columns[$orderConfig[0]['column']];
        $orderDir = ($orderConfig[0]['dir'] == 'desc') ? SORT_DESC : SORT_ASC;

        //Build query
        $contact = Contact::find();

        $recordsFilteredCount = $contact->count();     //count filtered records
        $contact = $contact->asArray()
            ->orderBy([$orderColumn => $orderDir])
            ->limit(Yii::$app->request->post('length'))
            ->offset(Yii::$app->request->post('start'))
            ->all();

        $data = [];

        if (!empty($contact)) {
            foreach ($contact as $key => $item) {
                $data[$key] = [
                    'name'     => $item['name'],
                    'email'    => $item['email'],
                    'phone'    => $item['phone'],
                    'website'  => $item['website'],
                    'message'  => $item['message'],
                    'date_add' => date('d.m.Y', strtotime($item['created_at'])),
                    'viewed'   => $this->render('partial/viewed', $item),
                    'actions'  => $this->render('partial/actions', $item)
                ];
            }
        }

        $output['data'] = $data;
        $output['recordsTotal'] = Contact::find()->count();
        $output['recordsFiltered'] = $recordsFilteredCount;

        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        return $output;
    }

    /**
     * Make news must read
     * @param int $id
     * @return array
     */
    public function actionIsViewed($id = 0)
    {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        $contact = Contact::findOne($id);

        if (empty($contact)) {
            return [
                'status'  => 'error',
                'message' => "Record was not found",
            ];
        }

        if ($contact->viewed) {
            $contact->viewed = 0;
            $message = 'Contact is not "viewed"';

        } else {
            $contact->viewed = 1;
            $message = 'Contact was made "viewed"';
        }

        $contact->save();

        return [
            'status'  => 'success',
            'message' => $message,
        ];
    }

    /**
     * Deletes an existing Contact model.
     * @param integer $id
     * @return mixed
     * @throws StaleObjectException
     * @throws \Throwable
     */
    public function actionDelete($id)
    {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        $contact = Contact::findOne($id);

        if ($contact->delete()) {
            Yii::$app->response->statusCode = 200;

            return [
                'status'  => 'success',
                'message' => "Record was delete successfully",
            ];

        } else {
            Yii::$app->response->statusCode = 422;

            return [
                'status'  => 'error',
                'message' => "Record was not delete successfully",
            ];
        }
    }
}