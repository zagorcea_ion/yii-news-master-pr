<?php

namespace app\controllers;

use app\models\LoginForm;
use Yii;
use yii\web\Controller;

/**
 * AuthController implements the User (Login & Logout).
 */
class AuthController extends Controller 
{
    /**
     * User login page.
     * @return mixed
     */
    public function actionLogin()
    {

        $this->layout = false;
        
        if (!Yii::$app->user->isGuest) {
            return $this->redirect(['admin/news']);
        }

        $model = new LoginForm();
        
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->redirect(['admin/news']);
        }

        $model->password = '';
        return $this->render('login', ['model' => $model]);
    }

    public function actionLogout()
    {
        Yii::$app->user->logout();
        return $this->redirect(['/']);
    }

}